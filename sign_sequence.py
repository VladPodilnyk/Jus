import numpy as np
import matplotlib.pyplot as plt

code    = "+++---+--+-"
N       = len (code)
x       = np.linspace (0, N + 1, N + 2)
y       = np.zeros (N + 2)

for i in range (1, N + 1):
    y [i] = 1 if code [i - 1] == '+' else -1

fig = plt.figure ()

plt.step (x,  y, linewidth=6.0);
plt.title ("Sign sequence")
plt.grid (True)
plt.xlabel ('X')
plt.ylabel ('Y')
plt.xlim (0, N)

plt.show()
