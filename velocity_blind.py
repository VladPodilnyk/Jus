import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


c = 3 * 100 * 1000 * 1000;

minFrequencyHz = 140 * 1000 * 1000;
maxFrequencyHz = 180 * 1000 * 1000;
minWaveLengthM = c / maxFrequencyHz;
maxWaveLengthM = c / minFrequencyHz;
waveLengthM = np.linspace (minWaveLengthM, maxWaveLengthM, 100);

minPulseRateHz = 250;
maxPulseRateHz = 720;
pulseRateHz = np.linspace (minPulseRateHz, maxPulseRateHz, 200);

[waveLength, pulseRate] = np.meshgrid (waveLengthM, pulseRateHz);

k = 1;

velocityBlind = k * waveLength * pulseRate / 2.0;


fig = plt.figure ()
ax = fig.gca (projection = '3d')
surf = ax.plot_surface (waveLength, pulseRate, velocityBlind,
                        rstride = 1, cstride = 1, cmap = cm.jet,
                        linewidth = 0, antialiased = False)

plt.title("Blind speed")
ax.set_xlabel("Wave length")
ax.set_ylabel("Pulse repetition frequency")
ax.set_zlabel("Velocity")

plt.show()
