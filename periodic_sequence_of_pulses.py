import numpy as np
import matplotlib.pyplot as plt


time_of_view        = 1.; # s.
dynamic_range       = 1.;

sampling_rate       = 1000.; # Hz
sampling_period     = 1. / sampling_rate; # s
sample_number       = time_of_view / sampling_period;
sampling_time       = np.linspace (0, time_of_view, sample_number);

carrier_frequency   = 50.;
amplitude           = 1;
phase               = 0;

pulse_shift         = 0.15;
pulse_width         = 0.09;
pulse_period        = 0.23;


sampling_signal     = np.zeros (sampling_time.size);
for i in range (sampling_time.size - 1):
    t = sampling_time [i] % pulse_period
    if t >= pulse_shift and t <= pulse_shift + pulse_width:
        sampling_signal [i] = amplitude * np.cos (2 * np.pi * carrier_frequency * sampling_time [i] + phase)
    else:
        sampling_signal [i] = 0


fig = plt.figure ()
plt.plot (sampling_time, sampling_signal);
plt.title("Periodic sequence of pulses")
plt.xlabel("Time")
plt.ylabel("Amplitude")

plt.show()