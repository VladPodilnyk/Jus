import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.fftpack

time_of_view            = 1e-3
dynamic_range           = 1.
carrier_frequency       = 30e6
sampling_rate           = 120e6
sample_number           = time_of_view * sampling_rate
sampling_time           = np.linspace (0, time_of_view, sample_number)
phase                   = 0.

pulse_width             = 3e-4
pulse_amplitude         = 1.
pulse_shift             = pulse_width / 0.8
pulse_period            = pulse_width * 3.
frequency_deviation     = 2e5
frequency_speed         = frequency_deviation / pulse_width
frequency_begin         = carrier_frequency - frequency_deviation / 2.
frequency_end           = carrier_frequency + frequency_deviation / 2.
frequency_dopler        = 0

quantizing_bits         = 8;
quantizing_levels       = 2 ** quantizing_bits / 2
quantizing_step         = dynamic_range / quantizing_levels


sampling_signal         = np.zeros (sampling_time.size);
sampling_signal         = np.random.normal (0, 0.01, sampling_time.size);
sampling_filter         = np.zeros (sampling_time.size);


for i in range (sampling_time.size):
    t = sampling_time [i] % pulse_period
    if t >= pulse_shift and t < pulse_shift + pulse_width:
        t -= pulse_shift
        sampling_signal [i] += pulse_amplitude * np.cos (2 * np.pi * ( (frequency_begin + frequency_dopler) * sampling_time [i] + frequency_speed / 2 * t ** 2) + phase)

for i in range (sampling_time.size):
    t = sampling_time [i]
    if t >= time_of_view - pulse_width:
        t -= (time_of_view - pulse_width)
        sampling_filter [i] = np.cos (2 * np.pi * (frequency_end * t - frequency_speed / 2 * t ** 2) + phase)
        # or create direct signal and just reverse it, like: sampling_filter = sampling_filter [::-1]

quantizing_signal       = np.round (sampling_signal / quantizing_step) * quantizing_step;


signal_i = +quantizing_signal * np.cos (2 * np.pi * carrier_frequency * sampling_time);
signal_q = -quantizing_signal * np.sin (2 * np.pi * carrier_frequency * sampling_time);

filter_i = +sampling_filter * np.cos (2 * np.pi * carrier_frequency * sampling_time);
filter_q = -sampling_filter * np.sin (2 * np.pi * carrier_frequency * sampling_time);


decimation_factor       = [3, 4, 4, 5]
decimated_time          = sampling_time
decimated_signal_i      = signal_i
decimated_signal_q      = signal_q
decimated_filter_i      = filter_i
decimated_filter_q      = filter_q

for i in range (len (decimation_factor) ):
    decimated_time      = decimated_time [0::decimation_factor [i] ];
    decimated_signal_i  = sig.decimate (decimated_signal_i, decimation_factor [i], 1, ftype="fir", axis=-1);
    decimated_signal_q  = sig.decimate (decimated_signal_q, decimation_factor [i], 1, ftype="fir", axis=-1);
    decimated_filter_i  = sig.decimate (decimated_filter_i, decimation_factor [i], 1, ftype="fir", axis=-1);
    decimated_filter_q  = sig.decimate (decimated_filter_q, decimation_factor [i], 1, ftype="fir", axis=-1);


decimated_signal_complex= decimated_signal_i + 1j * decimated_signal_q
decimated_filter_complex= decimated_filter_i + 1j * decimated_filter_q


decimation_rate         = np.prod (np.array (decimation_factor) )
sample_number_decimated = sample_number / decimation_rate
spectrum_signal         = scipy.fftpack.fft (decimated_signal_complex);
spectrum_filter         = scipy.fftpack.fft (decimated_filter_complex);
spectrum_compressed     = spectrum_signal * spectrum_filter

signal_compressed       = scipy.fftpack.ifft (spectrum_compressed)

magnitude_compressed    = np.zeros (decimated_time.size)
for i in range (signal_compressed.size):
    magnitude_compressed [i] = np.sqrt (signal_compressed [i].real ** 2 + signal_compressed [i].imag ** 2)


fig = plt.figure ()

plt.subplot (2, 1, 1)
plt.plot (sampling_time,  quantizing_signal);
plt.title ("Input signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")

plt.subplot (2, 1, 2)
plt.plot (decimated_time, magnitude_compressed);
plt.title ("Magnitude of compressed signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")

plt.show()
