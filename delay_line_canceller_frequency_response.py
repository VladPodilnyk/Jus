import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


w = np.linspace (0, 2 * np.pi, 100);
z = np.exp (-1j * w);

H_z = 1 - z ** -1;
#H_w = 2 * np.abs (np.sin (w / 2.) );
#mag = np.sqrt (np.real (H_z) ** 2 + np.imag (H_z) ** 2);


fig = plt.figure ()
ax = fig.gca (projection='3d')
ax.plot (w, np.real  (H_z),  np.imag (H_z), color='b', linewidth=2., label="H (z)")
ax.plot (w, np.real  (H_z),          w * 0, color='r', linewidth=1., label="Re {H (z)}")
ax.plot (w,          w * 0, np.imag  (H_z), color='g', linewidth=1., label="Im {H (z)}")
ax.plot (w, np.abs   (H_z),                 color='m', linewidth=1., label="|H (z)|")
ax.plot (w, np.angle (H_z),                 color='c', linewidth=1., label="angle {H (z)}")
ax.plot (w,          w * 0,                 color='k', linewidth=1.)
#ax.plot (w,            H_w,                 color='y', linewidth=1.)
#ax.plot (w,            mag,                 color='y', linewidth=1.)
ax.view_init (17.5, 220);
plt.xticks ([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi],
            [r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$'])

plt.title("Frequency response of the delay line canceler.")
ax.set_xlabel("Angular frequency.")
ax.set_ylabel("Real part.")
ax.set_zlabel("Imaginary part.")
plt.legend (loc="upper right")

plt.show()
