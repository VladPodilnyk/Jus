import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


kmHtoMs = 1000. / (60 * 60)
minVelocityKmH = 0.
maxVelocityKmH = 1200.
minVelocityMs = minVelocityKmH * kmHtoMs
maxVelocityMs = maxVelocityKmH * kmHtoMs
velocityMs = np.linspace (minVelocityMs, maxVelocityMs, 100)

minFrequencyHz = 140. * 1000 * 1000
maxFrequencyHz = 180. * 1000 * 1000
frequencyHz = np.linspace (minFrequencyHz, maxFrequencyHz, 100)

c = 3. * 100 * 1000 * 1000

[velocity, frequency] = np.meshgrid (velocityMs, frequencyHz)

dopler = (2. * velocity * frequency) / c


fig = plt.figure ()
ax = fig.gca (projection = '3d')
surf = ax.plot_surface (velocity, frequency, dopler,
                        rstride = 1, cstride = 1, cmap = cm.jet,
                        linewidth = 0, antialiased = False)

plt.title("Dopler frequency range")
ax.set_xlabel("Radial target velocity")
ax.set_ylabel("Probing frequency")
ax.set_zlabel("Dopler frequency")

plt.show()
