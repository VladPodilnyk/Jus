import numpy as np
import matplotlib.pyplot as plt

sample_rate     = 1e3
code            = "+++++--++-+-+"
time_of_view    = 1
bit_width       = time_of_view / len (code)
time            = np.linspace (0, time_of_view, time_of_view * sample_rate)
carrier         = len (code)
signal          = np.zeros (time.size)

for i in range (time.size):
    if time [i] < bit_width * len (code):
        j = int (time [i] / bit_width)
        phase = 0 if code [j] == '+' else np.pi
        signal [i] += np.sin (2 * np.pi * carrier * time [i] + phase)

fig = plt.figure ()

plt.plot (time,  signal);
plt.title ("BPSK signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")

plt.show()
