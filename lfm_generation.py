import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.fftpack

time_of_view            = 1e-3
dynamic_range           = 1.
carrier_frequency       = 30e6
sampling_rate           = 120e6
sample_number           = time_of_view * sampling_rate
sampling_time           = np.linspace (0, time_of_view, sample_number)
phase                   = 0.

pulse_width             = 2e-4
pulse_amplitude         = 1.
pulse_shift             = (time_of_view - pulse_width) / 2.
pulse_period            = 1
frequency_deviation     = 2e5
frequency_speed         = frequency_deviation / pulse_width
frequency_begin         = carrier_frequency - frequency_deviation / 2.
frequency_end           = carrier_frequency + frequency_deviation / 2.

quantizing_bits         = 8;
quantizing_levels       = 2 ** quantizing_bits / 2
quantizing_step         = dynamic_range / quantizing_levels


sampling_signal         = np.zeros (sampling_time.size);
#sampling_signal         = np.random.normal (0, 0.01, sampling_time.size);

for i in range (sampling_time.size):
    t = sampling_time [i] % pulse_period
    if t >= pulse_shift and t < pulse_shift + pulse_width:
        t -= pulse_shift
        sampling_signal [i] += pulse_amplitude * np.cos (2 * np.pi * (frequency_begin * sampling_time [i] + frequency_speed / 2 * t ** 2) + phase)


quantizing_signal       = np.round (sampling_signal / quantizing_step) * quantizing_step;


signal_i = +quantizing_signal * np.cos (2 * np.pi * carrier_frequency * sampling_time);
signal_q = -quantizing_signal * np.sin (2 * np.pi * carrier_frequency * sampling_time);


decimation_factor       = [3, 4, 4, 5]
decimated_time          = sampling_time
decimated_signal_i      = signal_i
decimated_signal_q      = signal_q

for i in range (len (decimation_factor) ):
    decimated_time      = decimated_time [0::decimation_factor [i] ];
    decimated_signal_i  = sig.decimate (decimated_signal_i, decimation_factor [i], 1, ftype="fir", axis=-1);
    decimated_signal_q  = sig.decimate (decimated_signal_q, decimation_factor [i], 1, ftype="fir", axis=-1);


signal_magnitude        = np.sqrt (decimated_signal_i ** 2 + decimated_signal_q ** 2);
signal_phase            = np.arctan2 (decimated_signal_q, decimated_signal_i);


spectrum_input          = scipy.fftpack.fft (sampling_signal);
spectrum_input          = spectrum_input [0: int (sample_number / 2)]
spectrum_input          = (2. / sample_number) * spectrum_input
spectrum_input          = np.abs (spectrum_input);
spectrum_input          = spectrum_input ** 2
frequency_input_nyquist = sampling_rate / 2.
frequency_input         = np.linspace (0., frequency_input_nyquist, sample_number / 2)

decimated_signal_complex= decimated_signal_i + 1j * decimated_signal_q
decimation_rate         = np.prod (np.array (decimation_factor) )
sample_number_decimated = sample_number / decimation_rate
spectrum_output         = scipy.fftpack.fft (decimated_signal_complex);
spectrum_output         = spectrum_output [0: int (sample_number_decimated / 2)]
spectrum_output         = (2. / sample_number_decimated) * spectrum_output
spectrum_output         = np.abs (spectrum_output);
spectrum_output         = spectrum_output ** 2
frequency_output_nyquist= sampling_rate / (2. * decimation_rate)
frequency_output        = np.linspace (0., frequency_output_nyquist, sample_number_decimated / 2)


fig = plt.figure ()

plt.plot (sampling_time,  quantizing_signal);
plt.title ("Input signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")


fig = plt.figure ()

plt.subplot (1, 2, 1)
plt.plot (decimated_time, signal_magnitude);
plt.title ("Magnitude of the signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")

plt.subplot (1, 2, 2)
plt.plot (decimated_time, signal_phase, 'g');
plt.title ("Phase of the signal.")
plt.xlabel ("Time")
plt.ylabel ("Phase")
plt.yticks ([ -np.pi,    -np.pi/2,      0,    np.pi/2,    np.pi],
            [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])


fig = plt.figure ()

plt.subplot (2, 1, 1)
plt.semilogy (frequency_input, spectrum_input, 'r');
plt.title ("Spectrum of input signal")
plt.xlabel ("Frequency")
plt.ylabel ("Power $|F (x)|^2$")
plt.grid ()

plt.subplot (2, 1, 2)
plt.semilogy (frequency_output, spectrum_output, 'r');
plt.title ("Spectrum of output signal")
plt.xlabel ("Frequency")
plt.ylabel ("Power $|F (x)|^2$")
plt.grid ()

plt.show()
